# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# load shared modules
define module_loader
	module purge; \
	module load sw/bio/emboss/6.4.0; \
	module load sw/bio/genometools/1.4.1; \
	module load lang/perl/5.10.1; \
	module load sw/bio/bedtools/2.16.2; \
	module load sw/bio/iga_tools/default; \
	module load sw/bio/samtools/0.1.19; \
	module load repeats/repeatmasker/open-4.0.3   * use rmblast *
endef

# true if maize_b73
TEST := false

GENOME := /genomes/zea_mays/assembly/reference/v3/zea_mays_v3.fasta
REF_FASTA := maize_TE_db.LTR.fasta.gz
TE_REPBASE :=  ../../db/Vitis_VV_RB_28_10_13.fasta.gz


TRNA := http://gtrnadb.ucsc.edu/Athal/Athal-tRNAs.fa
SUB_RATE := 1.3e-08
EXTENSION := 400
DISTMAT_METHOD := 0

# same as deletions2
define ltr_finder_param
-D 25000 \   * Max distance between LTRS, default is 20000 *
-d 100 \   * Min distance between LTRs, default is 1000 *
-L 6000 \   * Max LTR Length, default is 3500 *
-l 50 \   * Min LTR Length, default is 100 *
-S 5 \   * Output score threshold, (integer) *
-C \   * mask highly repeated regions *
-p 15 \   * Length of exact match pairs, (positive integer) *
-s $1 \   * predict PBS by using which trna database - all.trna.fasta *
-a $2 \   * Use ps_scan to predict IN(core), IN(c-term) and RH, dirname *
-F 11110000000 \   * TG and CA at 3' end of 5' of both LTRs *
-w 2   * output format: table. *
endef

# other specific targets ####

# download from http://maizetedb.org
# add fakes genomic coordinates
maize_TE_db.LTR.fasta.gz:
	wget -q \
	-O- \
	http://maizetedb.org/~maize/TE_12-Feb-2015_15-35.fa \
	| fasta2tab \
	| bawk 'BEGIN { i=1; } \
	!/^[$$,\#+]/ \
	{ if ($$1 ~ /RL.\_/) { \   * select only RL? *
	chr_name="chr1:"i"-"i+999; \
	print chr_name, toupper($$2); \
	print $$1, chr_name >"maize_TE_db.map"; \
	i+=1000; } \
	}' \
	| bsort -k1,1 \
	| tab2fasta 2 \
	| gzip -c >$@

# map original names to fake genomic coordinates
maize_TE_db.map: maize_TE_db.LTR.fasta.gz
	touch $@

# re-convert fake genomic coordinates to original names
maize.ltr_finder.dates: ltr_finder.abs.dst.sort maize_TE_db.map
	sed 's/-//g' $< \
	| bawk '!/^[\#+,$$]/ { \
	printf "%s:%i-%i\t%3.4f\n", $$1, $$2, $$3, $$8; }' \
	| translate -f 2 $^2 1 \
	| bsort -r -k 2,2 \
	| tr '_' \\t >$@

ALL += maize_TE_db.LTR.fasta.gz \
	maize_TE_db.map \
	maize.ltr_finder.dates