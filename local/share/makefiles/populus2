# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# load shared modules
define module_loader
	module purge; \
	module load sw/bio/emboss/6.4.0; \
	module load sw/bio/genometools/1.4.1; \
	module load lang/perl/5.10.1; \
	module load sw/bio/bedtools/2.16.2; \
	module load sw/bio/iga_tools/default; \
	module load sw/bio/samtools/0.1.19; \
	module load sw/graphics/circos/0.62-1; \
	module load sw/bio/repeatmasker/4.0.3   * use rmblast *
endef

GENOME := 
REF_FASTA := 
TE_REPBASE := 
TRACK_TITLE := POPOLUS
FREQUENCY := 
CIRCOS_TRACK := 


TRNA := http://gtrnadb.ucsc.edu/Athal/Athal-tRNAs.fa
SUB_RATE := 4.72e-09
EXTENSION := 400
BORDER_DISTANCE := 500

define ltr_finder_param
-D 25000 \   * Max distance between LTRS, default is 20000 *
-d 100 \   * Min distance between LTRs, default is 1000 *
-L 6000 \   * Max LTR Length, default is 3500 *
-l 50 \   * Min LTR Length, default is 100 *
-E \   * LTR must have edge signal (at least two of PBS,PPT,TSR) *
-S 5 \   * Output score threshold, (integer) *
-C \   * mask highly repeated regions *
-p 15 \   * Length of exact match pairs, (positive integer) *
-s $1 \   * predict PBS by using which trna database - all.trna.fasta *
-a $2 \   * Use ps_scan to predict IN(core), IN(c-term) and RH, dirname *
-F 11110000000 \   * Filter to choose desired result,default is 0 *
-w 2   * output format: table. *
endef

#################################

TOT_LTR_FINDER_FASTA := /projects/populus/ep/Heterosis/data/LTR/LTR_v3_sequences_new.fasta

TOT_LTR_FINDER_TAB := /projects/populus/ep/Heterosis/data/LTR/LTR_v3_coordinates_new.txt

# ltr_finder.fasta:
	# fasta2tab <$(TOT_LTR_FINDER_FASTA) \
	# | bsort -k 1,1 \   * guarantees to give the same numbers in ltr_finder.tab rule *
	# | bawk 'BEGIN { i=1; } \
	# !/^[$$,\#+]/ \
	# { chr_name="chr1:"i"-"i+999; \
	# print chr_name, toupper($$2); \
	# print $$1"_"i, chr_name >"populus2_TE_db.map"; \   * add number to transposon name to avoid duplication of names *
	# i+=1000; }' \
	# | bsort -k1,1 \
	# | tab2fasta 2 >$@

ltr_finder.fasta:
	fasta2tab <$(TOT_LTR_FINDER_FASTA) \
	| bawk 'BEGIN { i=1; } \
	!/^[$$,\#+]/ \
	{ chr_name="chr1:"i"-"i+999; \
	print chr_name, toupper($$2); \
	print $$1, chr_name >"populus_TE_db.map"; \
	i+=1000; }' \
	| bsort -k1,1 \
	| tab2fasta 2 >$@

populus_TE_db.map: ltr_finder.fasta
	touch $@


.META: ltr_finder.tab
	1	Name
	2	Superfamily
	3	start 5'-LTR
	4	end 5'-LTR
	5	start 3'-LTR
	6	end 3'-LTR

# # some LTR coordinates exceeded the corresponding
# # sequence length. Replace
# ltr_finder.tab: populus2_TE_db.map
	# unhead <$(TOT_LTR_FINDER_TAB) \
	# | bsort -k 1,1 \
	# | awk 'BEGIN { i=1; } !/^[$$,\#+]/ \
	# { printf "%s_%i\t",$$1,i; \
	# for (j=2; j<=NF; j++) printf "%s\t",$$j; \
	# printf "%s\n",$$NF; \
	# i+=1000; }' \
	# | translate $< 1 >$@

# some LTR coordinates exceeded the corresponding
# sequence length. Replace
ltr_finder.tab: populus_TE_db.map
	unhead <$(TOT_LTR_FINDER_TAB) \
	| translate $< 1 >$@




# By default, the FASTA header for each extracted sequence
# will be formatted as follows: “<chrom>:<start>-<end>”.
ltr_finder.ltr1: ltr_finder.tab ltr_finder.fasta
	$(call module_loader); \
	>$@; \
	cut -f 1,3,4 $< \
	| sortBed -i stdin \
	| bedtools getfasta -fi $^2 -bed stdin -tab -fo stdout \
	| bawk '!/^[\#+,$$]/ { gsub(":",";",$$1); print $$1, $$2; }' \
	| tab2fasta -s 2 >$@

ltr_finder.ltr2: ltr_finder.tab ltr_finder.fasta
	$(call module_loader); \
	>$@; \
	cut -f 1,5,6 $< \
	| sortBed -i stdin \
	| bedtools getfasta -fi $^2 -bed stdin -tab -fo stdout \
	| bawk '!/^[\#+,$$]/ { gsub(":",";",$$1); print $$1, $$2; }' \
	| tab2fasta -s 2 >$@


# re-convert fake genomic coordinates to original names
populus.ltr_finder.dates: ltr_finder.abs.dst.sort ltr_finder.tab populus_TE_db.map
	sed 's/-//g' $< \
	| bawk '!/^[\#+,$$]/ { \
	printf "%s:%i-%i\t%3.4f\t%3.4f\n", $$1, $$2, $$3, $$8, $$9; }' \
	| translate -a $^2 1 \
	| select_columns 1 2 7 8 \
	| translate -f 2 $^3 1 \
	| bsort -r -k 2,2 -k 3,3 >$@



remains2extended: ltr_finder.tab
	paste \
	<(cut -f1 $<) \
	<(cut -f1 $<) >$@

ALL += ltr_finder.fasta \
	populus_TE_db.map \
	ltr_finder.ltr1 \
	ltr_finder.ltr2
