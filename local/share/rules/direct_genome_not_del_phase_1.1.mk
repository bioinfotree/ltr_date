# Copyright 2016 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

LOCAL_REFERENCE ?=
CHUNCK_SIZE ?= 5000000
MAX_LTRS_DIST ?= 20000
MIN_LTRS_DIST ?= 1000
MAX_LTR_LEN ?= 3500
MIN_LTR_LEN ?= 100
TRNA ?=


# reference.fasta is created by supra-makefile
split_fasta.mk: reference.fasta
	$(call module_loader); \
	splitter \
	-sformat fasta \
	-filter \
	-size=$(CHUNCK_SIZE) \
	-overlap=$$(echo "500 + $(MAX_LTRS_DIST) + 2 * $(MAX_LTR_LEN)" | bc) \   * overlap is calculated on  the basis of maax distance between ltrs and max length of ltrs plus some bp *
	-ossingle $<; \
	printf "CHUNCKS := \$$(wildcard *_*-*.fasta)">$@

# needed for initialize variablee $(CHUNCKS)
include split_fasta.mk


LTR_FINDERS = $(CHUNCKS:.fasta=.ltr_finder)
%.ltr_finder: %.fasta tRNA.fasta ps_scan split_fasta.mk
	ltr_finder \
	$(call ltr_finder_param) \
	-s $^2 \   * tRNA sequence file *
	$^1 >$@



.META: %.ltr_finder
	1	index
	2	SeqID
	3	LTR1_start
	4	LTR2_end
	5	LTR1_len
	6	LTR2_len
	7	Inserted element len
	8	TSR
	9	PBS
	10	PPT
	11	RT
	12	IN (core)
	13	IN (c-term)
	14	RH
	15	Strand
	16	Score
	17	Sharpness
	18	Similarity

TABS = $(LTR_FINDERS:.ltr_finder=.tab)
%.tab: %.ltr_finder
	gawk -F'\t' 'BEGIN{OFS="\t";} /^\[.[0-9]+\]\t/ { \
	  out=""; \
	  gsub(/[\[+,\]+,[:blank:]]/,"",$$1); \
	  # print 1 2 \
	  printf "%s\t%s",$$1,$$2; \
	  split($$3,b,"-"); \
	  # print 3 \
	  for(i=1;i<=length(b);i++) {printf "\t%s",b[i]}; \
	  split($$4,c,","); \
	  # print 4 \
	  for(i=1;i<=length(c);i++) {printf "\t%s",c[i]}; \
	  # print rest \
	  for(i=5;i<=NF;i++) {printf "\t%s",$$i}; \
	  printf "\n"; } ' <$< >$@


# # put all together
# tab.all: $(TABS)
	# $(call module_loader); \
	# cat $^ \
	# | bawk 'BEGIN { i=0; } \
	# !/^[$$,\#+]/ { \
	# i++ \
	# split($$2,b,/[_-]/); \
	# split($$9,a,"-"); \
	# split($$10,c,"-"); \
	# print i, b[1], $$3+b[2]-1, $$4+b[2]-1, $$5, $$6, $$7, $$8, a[1]+b[2]-1"-"a[2]+b[2]-1, c[1]+b[2]-1"-"c[2]+b[2]-1, $$11, $$12, $$13, $$14, $$15, $$16, $$17, $$18; }' \
	# | bsort -k3,3n \
	# | bawk '!/^[$$,\#+]/ { LINE=$$0; gsub("\t"," ",LINE); print $$2, $$3, $$4, LINE }' \
	# | bedtools merge -i stdin -c 4 -o distinct -delim ";" \
	# | cut -f4 \
	# | cut -d  ";" -f 1 \
	# | tr ' ' "\t" >$@


tab.all: $(TABS)
	$(call module_loader); \
	cat $^ \
	| bawk '!/^[$$,\#+]/ { LINE=$$0; gsub("\t"," ",LINE); print $$2, $$3, $$4, LINE }' \
	| bsort -k1,1 -k2,2n \
	| bedtools merge -i stdin -c 4 -o distinct -delim ";" \
	| cut -f4 \
	| cut -d  ";" -f 1 \
	| tr ' ' "\t" >$@


.PHONY: test
test:
	@echo


ALL += reference.fasta \
	tRNA.fasta \
	split_fasta.mk \
	$(CHUNCKS) \
	$(LTR_FINDERS) \
	$(TABS) \
	tab.all

CLEAN += $(CHUNCKS) \
	 $(LTR_FINDERS) \
	 $(TABS)

INTERMEDIATE +=
