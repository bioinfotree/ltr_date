# Copyright 2013 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

MAKE := bmake
TRNA ?=
CHUNCK_SIZE ?= 5000000
MAX_LTRS_DIST ?= 20000
MAX_LTR_LEN ?= 3500


reference.fasta:
	ln -sf $(GENOME) $@

# change id of sequences to numbers
reference.norm.fasta: reference.fasta
	cat $< \
	| fasta2tab \
	| tr " " \\t \
	| awk 'BEGIN { OFS = "\t" } \
	!/^[\#+,$$]/ { \
	if ( $$1 ~ /chr[0-9]+/ ) print $$1,$$NF; }' \   * filter is needed by denom *
	| bsort \
	| tab2fasta 2 >$@

# # download tRNA sequences in fasta format
tRNA.fasta:
	wget -q -O $@ $(TRNA)


# create directory for ps_scan.
# LTR_FINDER can predict protein domains by calling ps_scan, which can be obtained from ExPASy-PROSITE
# (http://www.expasy.org/prosite/). User should place data file `prosite.dat' and ps_scan in this directory.
# If this parameter is enabled, LTR_FINDER will call them and report these protein domains if they are detected. 
ps_scan:
	mkdir -p $@; \
	cd $@; \
	wget -q -O prosite.dat ftp://ftp.expasy.org/databases/prosite/prosite.dat; \
	ln -sf $$PRJ_ROOT/local/bin/ps_scan ps_scan.pl; \
	cd ..

# execute ltr_finder for each chromosome by splitting
# each chromosome into overlapping fragments
all.ltr_finder.tab: makefile reference.norm.fasta tRNA.fasta ps_scan
	$(call module_loader); \
	RULES_PATH="$$PRJ_ROOT/local/share/rules/direct_genome_not_del_phase_1.1.mk"; \
	if [ -s $$RULES_PATH ] && [ -s $< ]; then \
	  >$@; \
	  >all.ltr_finder; \
	  FASTA_LST=$$(fasta2tab <$^2 | tr ' ' \\t | cut -f1 | tr [:upper:] [:lower:]); \
	  seqretsplit -filter -supper1 -sformat fasta <$^2; \
	  for SEQ in $$FASTA_LST; do \
	    DIR_NAME=$${SEQ%.*}; \
	    mkdir -p $$DIR_NAME; \
	    cd $$DIR_NAME; \
	    ln -sf ../$$SEQ.fasta reference.fasta; \
	    ln -sf ../$<; \
	    ln -sf $$RULES_PATH rules.mk; \
	    ln -sf ../$^3; \
	    ln -sf ../$^4; \
	    $(MAKE) && \
	    cat *.ltr_finder >>../all.ltr_finder; \
	    cat tab.all >>../$@; \
	    cd ..; \
	    rm -r $$DIR_NAME $$DIR_NAME.fasta; \
	  done; \
	else \
	  printf "[ ERROR ] $$RULES_PATH not found!\n" >&2; \
	  exit 1; \
	fi

all.ltr_finder: all.ltr_finder.tab
	touch $@

# re-enumerate first column to use for
# filtering purpose below
rel.ltr_finder.tab: all.ltr_finder.tab
	awk 'BEGIN { OFS = "\t"; n=0; } \
	!/^[$$,\#+]/ \
	{ n++; \
	printf "%i\t",n; \
	for( i=2;i<=NF-1;i++ ) \
		{ printf "%s\t",$$i; } \
	printf "%s\n",$$NF; }' <$< >$@

# calcolate absolute coordinates from relative to fragments
# and try to merge overlapping fragments
abs.merge.ltr_finder.tab: all.ltr_finder.tab
	$(call module_loader); \
	cat $< \
	| bawk 'BEGIN { i=0; } \
	!/^[$$,\#+]/ { \
	i++ \
	split($$2,b,/[_-]/); \
	split($$9,a,"-"); \
	split($$10,c,"-"); \
	print b[1], $$3+b[2]-1, $$4+b[2]-1, i" "b[1]" "$$3+b[2]-1" "$$4+b[2]-1" "$$5" "$$6" "$$7" "$$8" "a[1]+b[2]-1"-"a[2]+b[2]-1" "c[1]+b[2]-1"-"c[2]+b[2]-1" "$$11" "$$12" "$$13" "$$14" "$$15" "$$16" "$$17" "$$18; }' \
	| bsort -k1,1 -k2,2n \
	| bedtools merge -i stdin -c 4 -o distinct -delim ";" \
	| cut -f4 \
	| cut -d  ";" -f 1 \
	| tr ' ' "\t" >$@

# convert relative coordinates to absolute coordinate of chrs
# this is needed in order to filter for deletions
# which have absolute coordinates
abs.ltr_finder.tab: all.ltr_finder.tab
	$(call module_loader); \
	cat $< \
	| bawk 'BEGIN { i=0; } \
	!/^[$$,\#+]/ { \
	i++ \
	split($$2,b,/[_-]/); \
	split($$9,a,"-"); \
	split($$10,c,"-"); \
	print i, b[1], $$3+b[2]-1, $$4+b[2]-1, $$5, $$6, $$7, $$8, a[1]+b[2]-1"-"a[2]+b[2]-1, c[1]+b[2]-1"-"c[2]+b[2]-1, $$11, $$12, $$13, $$14, $$15, $$16, $$17, $$18; }' \
	| bsort -k1,1 -k2,2n >$@

# obtain a list of LTRs that do not overlap deletions
# use absolute coordinates because deletions are expressed in absolute coordinates too
# obtain a list with absolute coordinates and entry position
remains.ltr_finder.bed: abs.ltr_finder.tab
	$(call module_loader); \
	select_columns 2 3 4 1 <$< \
	| bedtools intersect -v -f 0.5 -bed -a stdin -b <( \
	zcat <$(ALL_DELETIONS) \
	| fasta2tab \
	| bawk '!/^[\#+,$$]/ { split($$1,a,/[-: ]/); print a[1], a[2], a[3], "DELETION"; }' \
	| bsort -k 1,1 -k 2,2n ) >$@

# for compatibility with already writen phase_2
# extract LTR sequences found by ltr_finder, from chrs, using absolute coordinates
tot.ori-extended.ltr_finder.fasta: abs.ltr_finder.tab reference.norm.fasta
	$(call module_loader); \
	select_columns 2 3 4 <$< \
	| bedtools getfasta -fi $^2 -bed stdin -tab -fo stdout \
	| tab2fasta -s 2 >$@

# for compatibility with already writen phase_2
# compose the names of the chromosomes using START and END absolute coordinates of LTRs
# change also START and END of LTR sequence accordingly
tot.ori-extended.ltr_finder.filtered.tab: abs.ltr_finder.tab remains.ltr_finder.bed
	filter_1col 1 <(cut -f 4 <$^2) <$< \
	| bawk '!/^[$$,\#+]/ { print $$1, $$2":"$$3"-"$$4, "0", $$4-$$3, $$5, $$6; }' >$@

# for compatibility with already writen phase_2
.META: remains2extended
	1	old coordinates
	2	new coordinates

remains2extended: tot.ori-extended.ltr_finder.filtered.tab
	select_columns 2 2 <$< \
	| bsort \
	| uniq >$@


.PHONY: test
test:


ALL += reference.fasta \
	tRNA.fasta \
	all.ltr_finder.tab \
	all.ltr_finder

CLEAN += $(wildcard *.fasta) \
	 ps_scan

# add dipendence to clean targhet
clean: clean_dir

.PHONY: clean_dir
clean_dir:
	@echo cleaning up...
	for D in $(DIR_LST); do \
	  if [ -d $$D ]; then \
	    cd $$D; \
	    $(MAKE) clean; \
	    cd ..; \
	  fi; \
	done


INTERMEDIATE += reference.norm.fasta