# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

#CIRCOS_ETC_DIR = $(shell echo $$(dirname $$($(call module_loader); which circos))/../etc)


define IDEOGRAM_CONF
<ideogram>

<spacing>
default = 0.005r
</spacing>

# Ideogram position, fill and outline
radius           = 0.90r
thickness        = 20p
fill             = yes
stroke_color     = dgrey
stroke_thickness = 2p

# Minimum definition for ideogram labels.

show_label       = yes
# see etc/fonts.conf for list of font names
label_font       = default 
label_radius     = dims(image,radius) - 60p
label_size       = 30
label_parallel   = yes

</ideogram>
endef
export IDEOGRAM_CONF
ideogram.conf:
	@echo "$$IDEOGRAM_CONF" >$@





define TICKS_CONF
show_ticks          = yes
show_tick_labels    = yes

<ticks>
radius           = 1r
color            = vvdred
thickness        = 2p

# the tick label is derived by multiplying the tick position
# by 'multiplier' and casting it in 'format':
#
# sprintf(format,position*multiplier)
#

multiplier       = 1e-6

# %d   - integer
# %f   - float
# %.1f - float with one decimal
# %.2f - float with two decimals
#
# for other formats, see http://perldoc.perl.org/functions/sprintf.html

format           = %d

<tick>
spacing        = 5u
size           = 10p
</tick>

<tick>
spacing        = 25u
size           = 15p
show_label     = yes
label_size     = 20p
label_offset   = 10p
format         = %d
</tick>

</ticks>
endef
export TICKS_CONF
ticks.conf:
	@echo "$$TICKS_CONF" >$@




define BACKGROUNDS_CONF
<backgrounds>
# Show the backgrounds only for ideograms that have data
show  = data
<background>
color = vvlgrey
</background>
<background>
color = vlgrey
y0    = 0.2r
y1    = 0.5r
</background>
<background>
color = lgrey
y0    = 0.5r
y1    = 0.8r
</background>
<background>
color = grey
y0    = 0.8r
</background>

</backgrounds>
endef
export BACKGROUNDS_CONF
backgrounds.conf:
	@echo "$$BACKGROUNDS_CONF" >$@





define RULE_EXCLUDE_CONF
#
# to avoid displaying any data on hs1. The rule is included from a
# file because it is reused again in the track below.
#<rule>
# condition = on(hs1)
# show      = no
#</rule>
endef
export RULE_EXCLUDE_CONF
rule.esclude.conf:
	@echo "$$RULE_EXCLUDE_CONF" >$@



# CIRCOS_TRACK is defined, CIRCOS_TRACK become the 2° track and frequency.circos become the 3° track.
# Otherwise frequency.circos become the 2° track and CIRCOS_TRACK will be not defined
ifdef CIRCOS_TRACK

define THIRD_TRACK_CONF
<plot>
type = histogram
file = frequency.circos

r0 = 0.40r
r1 = 0.59r

fill_color = vvdred
color  = vvdred
thickness = 4p
extend_bin = no
</plot>
endef
export THIRD_TRACK_CONF

endif

3track.conf:
	@echo "$$THIRD_TRACK_CONF" >$@ 




define CIRCOS_CONF
# Chromosome name, size and color definition
karyotype = karyotype.vitis.txt

chromosomes_units           = 250000
chromosomes_display_default = yes


<<include ideogram.conf>>
<<include ticks.conf>>
<<include backgrounds.conf>>

# The <ideogram> block defines the position, size, labels and other
# properties of the segments on which data are drawn. These segments
# are usually chromosomes, but can be any integer axis.

<ideogram>

<spacing>
# Spacing between ideograms. Suffix "r" denotes a relative value. It
# is relative to circle circumference (e.g. space is 0.5% of
# circumference).
default = 0.005r

# You can increase the spacing between specific ideograms.
#<pairwise hsY;hs1>
#spacing = 20r
#</pairwise>

</spacing>

# Ideogram position, thickness and fill. 
#
# Radial position within the image of the ideograms. This value is
# usually relative ("r" suffix).
radius           = 0.90r

# Thickness of ideograms, which can be absolute (e.g. pixels, "p"
# suffix) or relative ("r" suffix). When relative, it is a fraction of
# image radius.
thickness        = 80p

# Ideograms can be drawn as filled, outlined, or both. When filled,
# the color will be taken from the last field in the karyotype file,
# or set by chromosomes_colors. Color names are discussed in
#
# http://www.circos.ca/documentation/tutorials/configuration/configuration_files
#
# When stroke_thickness=0p or if the parameter is missing, the ideogram is
# has no outline and the value of stroke_color is not used.

fill             = yes
stroke_color     = dgrey
stroke_thickness = 2p

</ideogram>

################################################################
# The remaining content is standard and required. It is imported from
# default files in the Circos distribution.
#
# These should be present in every Circos configuration file and
# overridden as required. To see the content of these files, 
# look in etc/ in the Circos distribution.
#
# It's best to include these files using relative paths. This way, the
# files if not found under your current directory will be drawn from
# the Circos distribution. 
#
# As always, centralize all your inputs as much as possible.


# PLOTS START HERE
<plots>

<plot>
# The type sets the format of the track.
type = histogram
file = ltr_finder.abs.dst.circos

# The track is confined within r0/r1 radius limits. When using the
# relative "r" suffix, the values are relative to the position of the
# ideogram.
r0 = 0.80r
r1 = 0.99r

# Histograms can have both a fill and outline. The default outline is 1px thick black. 
fill_color = vvdblue
color  = vvdblue

# To turn off default outline, set the outline thickness to zero. If
# you want to permanently disable this default, edit
# etc/tracks/histogram.conf in the Circos distribution.

thickness = 4p

# Do not join histogram bins that do not abut.
extend_bin = no

# Like for links, rules are used to dynamically alter formatting of
# each data point (i.e. histogram bin). Here, I include the <rule>
# block from a file, which contains the following

<rules>
<<include rule.esclude.conf>>
</rules>
</plot>


<plot>
type = histogram
file = __SECONDTRACK__

r0 = 0.60r
r1 = 0.79r

fill_color = vvdpurple
color  = vvdpurple
thickness = 4p
extend_bin = no
</plot>


# third track plot
<<include 3track.conf>>


# TEXT PLOT START HERE
<plot>

type       = text
color      =  vvdgreen
label_font = mono
show_links = no
label_size   = 30p
# you can put in optional padding and rpadding to give the text margins
padding    = 0p

file       = track.title.txt
r0 = 0.20r
r1 = 0.39r
label_parallel = yes
# label_rotate = no
</plot>

</plots>
# PLOTS END HERE


<image>
# Included from Circos distribution.
<<include etc/image.conf>>
</image>

# RGB/HSV color definitions, color lists, location of fonts, fill
# patterns. Included from Circos distribution.
#
# In older versions of Circos, colors, fonts and patterns were
# included individually. Now, this is done from a central file. Make
# sure that you're not importing these values twice by having
#
# *** DO NOT DO THIS ***
# <colors>
# <<include etc/colors.conf>>
# <colors>
# **********************
<<include etc/colors_fonts_patterns.conf>> 

# Debugging, I/O an dother system parameters
# Included from Circos distribution.
<<include etc/housekeeping.conf>> 
endef
export CIRCOS_CONF



# if CIRCOS_TRACK us defined it become the second track, otherwise frequency.circos become the second
circos.conf: rule.esclude.conf ideogram.conf ticks.conf backgrounds.conf 3track.conf karyotype.vitis.txt frequency.circos ltr_finder.abs.dst.circos
ifdef CIRCOS_TRACK
	echo "$$CIRCOS_CONF" \
	| sed -e 's/\_\_SECONDTRACK\_\_/$(subst /,\/,$(CIRCOS_TRACK))/g' >$@
else
	echo "$$CIRCOS_CONF" \
	| sed -e 's/\_\_SECONDTRACK\_\_/frequency.circos/g' >$@
endif