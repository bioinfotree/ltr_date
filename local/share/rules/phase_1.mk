# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:
REF_FASTA ?= 
TE_REPBASE ?= 
TEST ?= false
RM_MODE ?= -qq
BORDER_DISTANCE ?= 1000000000
EXTENSION ?= 400

ifeq ($(TEST),true)
total.ref.fasta:
	zcat $(REF_FASTA) \
	| fasta2tab \
	| bawk '!/^[$$,\#+]/ \
	{ if ( $$1 ~ /\RLG/ || $$1 ~ /\RLC/ ) print $$1, toupper($$2); }' \
	| bsort \
	| tab2fasta 2 >$@
else
total.ref.fasta:
	zcat $(REF_FASTA) >$@
endif

te.db.fasta:
	zcat <$(TE_REPBASE) >$@

# masquerading soloLTR #########################################################

# extract LTR only
LTR.fasta: te.db.fasta
	cat $^ \
	| fasta2tab \
	| awk 'BEGIN { FS="\t"; OFS="\t" } !/^[$$,\#+]/ \
	{ if ($$1 ~ /LTR/) print $$1, toupper($$NF); }' \
	| bsort \
	| tab2fasta 2 >$@

.PRECIOUS: total.ref.fasta.masked
# mask the sequence of the deletion from the reference
# with the sequence of the reconstructed variant in the assembly
total.ref.fasta.masked: LTR.fasta total.ref.fasta
	!threads
	$(call module_loader); \
	RepeatMasker \
	$(RM_MODE) \
	-no_is \
	-nolow \
	-pa $$THREADNUM \
	-lib $< \
	$^2

ref.soloLTR.fasta: total.ref.fasta.masked
	fasta2tab <$< \
	| bawk -v short_aln="ref.fasta.lst" '!/^[$$,\#+]/ { \
	ncount = gsub("N","N",$$2); \
	if ( ncount >= length($$2)*0.90 ) print $$0; else print $$1 >short_aln }' \
	| bsort \
	| tab2fasta 2 >$@

ref.fasta.lst: ref.soloLTR.fasta
	touch $@

ref.fasta: ref.fasta.lst total.ref.fasta
	fasta_get -f $< <$^2 >$@

# solo LTR found #########################################################

# download tRNA sequences in fasta format
tRNA.fasta:
	ln -sf $$PRJ_ROOT/local/src/LTR_FINDER.x86_64-1.0.6/all.trna.fasta $@
	#wget -q -O $@ $(TRNA)


ps_scan:
	if [ ! -s $@.tar.gz ]; then \
		wget -q -O $@.tar.gz ftp://ftp.expasy.org/%2F/databases/prosite/ps_scan/ps_scan_linux_x86_elf.tar.gz; \
	fi \
	&& tar -xvzf $@.tar.gz \
	&& cd $@ \
	&& wget -q -O prosite.dat ftp://ftp.expasy.org/databases/prosite/prosite.dat; \
	cd ..



# split to multiple fasta in order to parallelize
CHUNCKS := $(addsuffix .ref.fasta,$(shell seq 8))
%.ref.fasta: ref.fasta
	$(call module_loader); \
	if [ $* == 1 ]; then \
		gt splitfasta -numfiles 8 -force yes $< \
		&& mv ref.fasta.$* $@; \
	else \
	while true; do \
		[ -f ref.fasta.$* ] && mv ref.fasta.$* $@ && break; \
		sleep 2; \
	done; \
	fi


LTR_FINDERS := $(CHUNCKS:.ref.fasta=.ref.ltr_finder)
%.ref.ltr_finder: %.ref.fasta tRNA.fasta ps_scan
	$(call module_loader); \
	ltr_finder \
	$(call ltr_finder_param,$^2,$^3) \
	$^1 >$@

ref.ltr_finder.tab: $(LTR_FINDERS)
	gawk -F'\t' 'BEGIN{OFS="\t";} /^\[.[0-9]+\]\t/ { \
	  out=""; \
	  gsub(/[\[+,\]+,[:blank:]]/,"",$$1); \
	  # print 1 2 \
	  printf "%s\t%s",$$1,$$2; \
	  split($$3,b,"-"); \
	  # print 3 \
	  for(i=1;i<=length(b);i++) {printf "\t%s",b[i]}; \
	  split($$4,c,","); \
	  # print 4 \
	  for(i=1;i<=length(c);i++) {printf "\t%s",c[i]}; \
	  # print rest \
	  for(i=5;i<=NF;i++) {printf "\t%s",$$i}; \
	  printf "\n"; } ' $^ >$@


ref.ltr_finder.lst: ref.ltr_finder.tab
	cut -f 2 $< \
	| sort -n \
	| uniq >$@


remains.fasta: ref.ltr_finder.lst ref.fasta
	fasta_get -f <(comm -23 <(fasta2tab <$^2 | cut -f1 | sort -n) $<) <$^2 >$@

# deletions extension #########################################################

remains.fasta.extended: remains.fasta
	$(call module_loader); \
	fasta2tab <$< \
	| cut -f1 \
	| tr ':-' \\t \
	| bawk '!/^[$$,\#+]/ { \
	NEW_LEFT=$$2 - $(EXTENSION); \
	NEW_RIGHT=$$3 + $(EXTENSION); \
	if ( NEW_LEFT < 0 ) { NEW_LEFT=0; } \   * in case we are substracting less then 0, stop to 0 coordinate *
	printf "%s:%s-%s\t%s:%s-%s\n", $$1, $$2, $$3, $$1, NEW_LEFT, NEW_RIGHT >"remains2extended"; \
	print $$1, NEW_LEFT, NEW_RIGHT; }' \
	| bedtools getfasta -fi $(GENOME) -bed - -fo $@

.META: remains2extended
	1	old coordinates
	2	new coordinates

remains2extended: remains.fasta.extended
	touch $@

# 2° round LTR_FINDER #########################################################

# split to multiple fasta in order to parallelize
CHUNCKS2 := $(addsuffix .remains.fasta.extended,$(shell seq 8))
%.remains.fasta.extended: remains.fasta.extended
	$(call module_loader); \
	if [ $* == 1 ]; then \
		gt splitfasta -numfiles 8 -force yes $< \
		&& mv remains.fasta.extended.$* $@; \
	else \
	while true; do \
		[ -f remains.fasta.extended.$* ] && mv remains.fasta.extended.$* $@ && break; \
		sleep 2; \
	done; \
	fi


LTR_FINDERS2 := $(CHUNCKS2:.remains.fasta.extended=.remains.extended.ltr_finder)
%.remains.extended.ltr_finder: %.remains.fasta.extended tRNA.fasta ps_scan
	$(call module_loader); \
	ltr_finder \
	$(call ltr_finder_param,$^2,$^3) \
	$^1 >$@

# contains extended coordinates
remains.extended.ltr_finder.tab: $(LTR_FINDERS2)
	gawk -F'\t' 'BEGIN{OFS="\t";} /^\[.[0-9]+\]\t/ { \
	  out=""; \
	  gsub(/[\[+,\]+,[:blank:]]/,"",$$1); \
	  # print 1 2 \
	  printf "%s\t%s",$$1,$$2; \
	  split($$3,b,"-"); \
	  # print 3 \
	  for(i=1;i<=length(b);i++) {printf "\t%s",b[i]}; \
	  split($$4,c,","); \
	  # print 4 \
	  for(i=1;i<=length(c);i++) {printf "\t%s",c[i]}; \
	  # print rest \
	  for(i=5;i<=NF;i++) {printf "\t%s",$$i}; \
	  printf "\n"; } ' $^ >$@

# contains LTR found with the extended coordinates BUT ORIGINAL COORDINATES OF SEQUENCES IN 2° COLUMN
remains.ltr_finder.tab: remains.extended.ltr_finder.tab remains2extended
	translate -f 2 $^2 2 <$< >$@

# get original coordinates for sequences in which LTR were found after extension
remains.extended.ltr_finder.lst: remains.extended.ltr_finder.tab remains2extended
	filter_1col 2 <(cut -f 2 $< | bsort | uniq) <$^2 \
	| cut -f1 >$@

# get new coordinates for sequences in which LTR were found after extension
remains.with.extended.ltr_finder.lst: remains.extended.ltr_finder.tab remains2extended
	filter_1col 2 <(cut -f 2 $< | bsort | uniq) <$^2 \
	| cut -f2 >$@

# sequences in which no LTR were found, in original coordinates
remains2.lst: remains.extended.ltr_finder.tab remains2extended 
	filter_1col -v 2 <(cut -f 2 $< | bsort | uniq) <$^2 \
	| cut -f1 >$@

remains2.fasta: remains2.lst remains.fasta
	fasta_get -f $< <$^2 >$@

# merge results #########################################################

.META: tot.ori-extended.ltr_finder.tab tot.ori.ltr_finder.tab remains.extended.ltr_finder.tab remains.ltr_finder.tab ref.ltr_finder.tab tot.ori-extended.ltr_finder.filtered.tab
	1	index
	2	SeqID
	3	LTR1_start
	4	LTR2_end
	5	LTR1_len
	6	LTR2_len
	7	Inserted element len
	8	TSR
	9	PBS
	10	PPT
	11	RT
	12	IN (core)
	13	IN (c-term)
	14	RH
	15	Strand
	16	Score
	17	Sharpness
	18	Similarity

# sequences (2° column) in which LTR were found both in first and second pass were reported with ORIGINAL COORDINATES
tot.ori.ltr_finder.tab: remains.ltr_finder.tab ref.ltr_finder.tab
	cat $^ \
	| bsort -k2,2 -k1,1g\
	| uniq >$@

# sequences (2° column) in which LTR were found both in first pass were reported with ORIGINAL coordinates.
# sequences in which LTR were found in second pass were reported with EXTENDED COORDINATES
tot.ori-extended.ltr_finder.tab: remains.extended.ltr_finder.tab ref.ltr_finder.tab
	cat $^ \
	| bsort -k 2,2 -k 1,1 \
	| uniq >$@


# SELECTION CRITERIA FOR BETTER LTR_FINDER MATCH:
# 1 max LTR1 length
# 2 max LTR2 length
# 3 max sharpness
# 4 max similarity
# 5 presence of TSR
# 6 max score

# select the more significant alignment of LTR_finder, on the basis of the longer LTR1
# this seems sufficient to select the best LTR_finder match
tot.ori-extended.ltr_finder.filtered.tab: tot.ori-extended.ltr_finder.tab
	bawk 'BEGIN { start=""; i=0; maxLTR1len=-1; delete a } \
	!/^[\#+,$$]/ { \
	if ( NR == 1 ) \
	{ \
		start=$$2; \
		i++; \
		longer[$$5]=$$0; \
		maxLTR1len=$$5; \
	} \
	else { \
		if ( $$2 != start ) \   * from the second line onwards perform tests between the current line and the precendet *
		{ \
			print i, longer[maxLTR1len]; \
			i=0; \   * reset array index *
			start=$$2; maxLTR1len=-1; delete a; \
		} \
			longer[$$5] = $$0; \
			if ( maxLTR1len < $$5 ) maxLTR1len=$$5; \
			i++; \
		} \
	} \
	END { \   * print data of last interval *
		print i, longer[maxLTR1len]; \
	}' $< \
	| cut -f1 --complement \
	| bawk 'function abs(x){return ((x < 0.0) ? -x : x)} \
	!/^[\#+,$$]/ { \
	split($$2,a,":|-"); \
	CHR_START = a[2]; \
	CHR_END = a[3]; \
	LTR1_REL_START = $$3 -1; \
	LTR2_END = CHR_START + $$4 -1 ; \
	if ( LTR1_REL_START <= $(BORDER_DISTANCE) && abs( CHR_END - LTR2_END ) <= $(BORDER_DISTANCE) ) print $$0  \   * filter for distance from the border of the LTRs, also extended *
	}' \
	| bsort -k 2,2 -k 1,1 >$@


tot.ori-extended.ltr_finder.fasta: ref.fasta ref.ltr_finder.lst remains.fasta.extended remains.with.extended.ltr_finder.lst
	if [ -s $< ] && [ -s $^2 ]; then \
		fasta_get -f $^2 <$< >$@; \
	fi; \
	if [ -s $^3 ] && [ -s $^4 ]; then \
		fasta_get -f $^4 <$^3 >>$@; \
	fi

tot.ori-extended.ltr_finder.count: LTR.fasta   ref.soloLTR.fasta   ref.fasta   ref.ltr_finder.lst    remains.extended.ltr_finder.lst    tot.ori-extended.ltr_finder.tab   tot.ori-extended.ltr_finder.filtered.tab
	paste -s \
	<(fasta_count -s $<) \
	<(fasta_count -s $^2) \
	<(fasta_count -s $^3) \
	<(wc -l $^4 | tr ' ' \\t) \
	<(wc -l $^5 | tr ' ' \\t) \
	<(cut -f2 $^6 | bsort | uniq | wc -l | sed 's/$$/\t$^6/g') \
	<(wc -l $^7 | tr ' ' \\t) \
	| select_columns 2 1 >$@



.PHONY: test
test:
	@echo $(CHUNCKS)

ALL += total.ref.fasta \
	total.ref.fasta.masked \
	ref.soloLTR.fasta \
	ref.fasta \
	te.db.fasta \
	tRNA.fasta \
	LTR.fasta \
	ps_scan \
	remains.fasta \
	ref.ltr_finder.lst \
	remains2.fasta \
	remains.extended.ltr_finder.lst \
	tot.ori-extended.ltr_finder.filtered.tab \
	tot.ori-extended.ltr_finder.fasta \
	tot.ori-extended.ltr_finder.count




CLEAN += $(CHUNCKS) \
	$(LTR_FINDERS) \
	$(CHUNCKS2) \
	$(LTR_FINDERS2) \
	ref.ltr_finder.tab \
	ref.ltr_finder.count \
	$(wildcard total.ref.fasta.*)

INTERMEDIATE += ps_scan.tar.gz \
	fasta.masked.unclassified.lst \
	remains.with.extended.ltr_finder.lst \
	remains2.lst