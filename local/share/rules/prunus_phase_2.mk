# Copyright 2015 Michele Vidotto <michele.vidotto@gmail.com>

# TODO:

SUB_RATE ?= 1.3e-08
# Multiple substitution correction methods for nucleotides:
# 0 (Uncorrected)
# 1 (Jukes-Cantor)
# 2 (Kimura Two parameters)
# 3 (Tamura)
# 4 (Tajima-Nei)
# 5 (Jin-Nei Gamma)
DISTMAT_METHOD ?= 2

extern ../phase_1/tot.ori-extended.ltr_finder.filtered.tab as TOT_LTR_FINDER_TAB
extern ../phase_1/tot.ori-extended.ltr_finder.fasta as TOT_LTR_FINDER_FASTA
extern ../phase_1/remains2extended as OLD_TO_NEW_COORD

# sequences (2° column) in which LTR were found both in first pass were reported with ORIGINAL coordinates.
# sequences in which LTR were found in second pass were reported with EXTENDED COORDINATES
ltr_finder.tab:
	ln -sf $(TOT_LTR_FINDER_TAB) $@

ltr_finder.fasta:
	ln -sf $(TOT_LTR_FINDER_FASTA) $@


ltr_finder.ltr1: ltr_finder.tab ltr_finder.fasta
	$(call module_loader); \
	>$@; \
	bawk '!/^[$$,\#+]/ { \
		start_1=$$3; \
		end_1=$$3+$$5; \
		print $$2, start_1, end_1; }' <$< \
	| bedtools getfasta -fi $^2 -bed stdin -tab -fo stdout \
	| bawk '!/^[\#+,$$]/ { gsub(":",";",$$1); print $$1, $$2; }' \
	| tab2fasta -s 2 >$@


ltr_finder.ltr2: ltr_finder.tab ltr_finder.fasta
	$(call module_loader); \
	>$@; \
	bawk '!/^[$$,\#+]/ { \
		start_2=$$4-$$6; \
		end_2=$$4; \
		print $$2, start_2, end_2; }' <$< \
	| bedtools getfasta -fi $^2 -bed stdin -tab -fo stdout \
	| bawk '!/^[\#+,$$]/ { gsub(":",";",$$1); print $$1, $$2; }' \
	| tab2fasta -s 2 >$@


ltr_finder.aln: ltr_finder.ltr1 ltr_finder.ltr2
	$(call module_loader); \
	>$@; \
	for i in $$(seq 0 $$(echo $$(fasta_count -s $< | cut -f1)-1 | bc)); do \
		if [ -s $< ] && [ -s $^2 ]; then \
			stretcher -sformat fasta \
			-filter \
			-asequence <(fasta_get @$$i <$<) \
			-bsequence <(fasta_get @$$i <$^2) \
			-aformat msf \
			>>$@; \
			printf "!!BLOCK_STOP\n" >>$@; \
	fi \
	done


# The output from the program is a file containing a matrix of the 
# calculated distances between each of the input aligned sequences.
# The distances are expressed in terms of the number of substitutions 
# per 100 bases or amino acids.
ltr_finder.mtx: ltr_finder.aln
	$(call module_loader); \
	{ \
		>$@; \
		read -r block; \   * First line only *
		while read -r; do \
			if [[ $$REPLY =~ ^!!BLOCK_STOP ]]; then \
				echo -e "$$block" | distmat -sformat1 msf -filter -nucmethod $(DISTMAT_METHOD) >>$@; \   * Do what you like with the contents *
				printf "!!BLOCK_STOP\n" >>$@; \
				block=""; \
			else \
				block+="\n$$REPLY"; \
			fi; \
		done; \
	 } <$<

LTR_ABSOLUTE = LTR1_START=a[2]-1; \
	LTR1_END=a[3]-1; \
	LTR2_START=b[2]-1; \
	LTR2_END=b[3]-1

LTR_RELATIVE = LTR1_START=a[2]-1; \
	LTR1_END=a[3]-1; \
	LTR2_START=b[2]-1; \
	LTR2_END=b[3]-1

#for ( x=1; x<length(a); x++ ) {printf a[x]"\t";}; \
#for ( y=1; y<length(a); y++ ) {printf b[y]"\t"}; \
#printf "\n"; \

define ltr_finder_dst
	$(call module_loader); \
	{ \
		>$@; \
		read -r block; \
		while read -r; do \
			if [[ $$REPLY =~ ^!!BLOCK_STOP ]]; then \
				echo -e "$$block" \
				| tr -s "\n" "\t" \
				| bawk '!/^[$$,\#+]/ { \
					gsub(/[ \t]+$$/, "", $$5); \   * remove spaces *
					SUB=$$5; \
					AGE=($$5*1e-08)/($(SUB_RATE)*2); \   * calculate time of divergence *
					split($$6,a,/[-; ]/); \   * remove sequence numbers *
					split($$8,b,/[-; ]/); \
					if ( a[1] != b[1] ) { print "ERROR $@: you are comparing LTRs from different sequences!" >"/dev/stderr"; exit 1; } \
					CHR=a[1]; \
					$1; \
					printf "%s\t%i\t%i\t%i\t%i\t%.4f\t%.4f\n", CHR, LTR1_START, LTR1_END, LTR2_START, LTR2_END, AGE, SUB; \
				}' >>$@; \
				block=""; \
			else \
				block+="$$REPLY"; \
			fi; \
		done; \
	 } <$2
endef

.META: ltr_finder.abs.dst ltr_finder.rel.dst ltr_finder.abs.dst.sort ltr_finder.rel.dst.sort
	1	chromosome	chr10
	2	sequence_start	10232746
	3	sequence_end	10245582
	4	LTR1_start	10232765
	5	LTR1_end	10234557
	6	LTR2_start	10243816
	7	LTR2_end	10245577
	8	age_MYA	0.75
	9	distance: number of substitutions per 100 bases	0.74

# AGE (Kdx*1e-02*1e-06)/(2*mu)
ltr_finder.abs.dst: ltr_finder.mtx
	$(call ltr_finder_dst,$(LTR_ABSOLUTE),$<)

ltr_finder.rel.dst: ltr_finder.mtx
	$(call ltr_finder_dst,$(LTR_RELATIVE),$<)

ltr_finder.abs.dst.sort: ltr_finder.abs.dst
	bsort -k1,1 -k2,2g <$< \
	| tr -d '-' >$@

ltr_finder.rel.dst.sort: ltr_finder.rel.dst
	bsort -k1,1 -k2,2g <$< \
	| tr -d '-' >$@

karyotype.vitis.txt:
	fasta_length <$(GENOME) \
	| bawk '!/^[\#+,$$]/ { print "chr - "$$1, $$1, "0", $$2, "black"; }' >$@

.META: ltr_finder.abs.dst.circos frequency.circos
	1	chr: chromosome	chr10
	2	start: LTR1_start	10232765
	3	end: LTR2_end	10245577
	4	value: age_MYA	0.75
	5	options:

ltr_finder.abs.dst.circos: ltr_finder.abs.dst.sort $(OLD_TO_NEW_COORD)
	TMP=$$(mktemp tmp.XXXXX); \
	sed 's/-//g' $< \
	| bawk '!/^[\#+,$$]/ { \
	printf "%s:%i-%i\t", $$1, $$2, $$3; \
	if ( $$8 > 10 ) { print "10"; } \   * cut max age a 10 MYA *
	else { printf "%3.4f\n", $$8; } }' \
	| translate -k -b $$TMP -f 2 $^2 1 \   * sequences in which LTRs are found after extension are reported in extended coordinates. Here traslate to original coordinates *
	| cat - $$TMP \
	| tr ':-' \\t \
	| bsort -k1,1 -k2,2g >$@ \
	&& rm $$TMP

ltr_finder.rel.dst.distrib.pdf: ltr_finder.abs.dst.circos
	cut -f4 $< \
	| distrib_plot \
	--type=histogram \
	--breaks=20 \
	--output=$@ \
	--remove-na \
	1

# get the number of samples in which  variants for which LTRs were dated, were found
frequency.circos: ltr_finder.abs.dst.circos
	bawk '!/^[\#+,$$]/ { printf "%s:%i-%i\t%s\n", $$1, $$2, $$3, $$4; }' $< \
	| bsort -k1,1 \
	| translate -v -e "0" -a -f 1 \   * some coordinates in $(FREQUENCY) are NOT present in original variant file, here sobstitute value with 0 *
	<(unhead <$(FREQUENCY) \
	| bawk '!/^[$$,\#+]/ { \
	N_elem = split($$62,a,";"); \
	print $$1, N_elem; }' \
	| bsort -k1,1) \
	1 \
	| cut -f 1,2 \
	| tr ':-' \\t \
	| bsort -k1,1 -k2,2g >$@


track.title.txt:
	echo -e "chrUn\t0\t43154196\t$(TRACK_TITLE)\n" >$@

# make configuration file for circos;
# contains recipe for "circos.conf"
import make_circos_conf

ltr_finder.abs.dst.png: circos.conf ltr_finder.abs.dst.circos
	$(call module_loader); \
	circos \
	-debug \
	-conf circos.conf \
	-png \
	-outputfile $@ \
	-outputdir .



.PHONY: test
test:
	@echo $(TRACK_TITLE)

ALL += ltr_finder.ltr1 \
	ltr_finder.ltr2 \
	ltr_finder.aln \
	ltr_finder.mtx \
	ltr_finder.abs.dst.sort \
	karyotype.vitis.txt \
	ltr_finder.abs.dst.circos \
	frequency.circos \
	circos.conf \
	rule.esclude.conf \
	ideogram.conf \
	ticks.conf \
	backgrounds.conf \
	track.title.txt \
	ltr_finder.abs.dst.png \
	ltr_finder.rel.dst.sort

INTERMEDIATE += ltr_finder.abs.dst \
	ltr_finder.rel.dst

CLEAN += 	ltr_finder.abs.dst.circos \
	circos.conf \
	rule.esclude.conf \
	ideogram.conf \
	ticks.conf \
	backgrounds.conf \
	track.title.txt